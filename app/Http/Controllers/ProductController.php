<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            $data = Product::select('id','name', 'price', 'details', 'publish')->orderBy('id', 'desc');

            return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('publish', function($row){
                return ($row->publish == 1 ? 'Yes' : 'No');
            })
            ->addColumn('action', function($row){
                return view('shared.actions', ['entity' => 'products', 'product' => $row->id ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'details' => 'required',
            'publish' => 'required',
        ]);

        $product = Product::create($validatedData);

        return redirect()->route('products.index')->with('success', 'Product created successfully');
       
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'details' => 'required',
            'publish' => 'required',
        ]);

        $product->update($validatedData);

        return redirect()->route('products.index')->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')->with('success', 'Product deleted successfully');
    }
}
