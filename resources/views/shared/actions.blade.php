<form class="delete" action="{{ route($entity.'.destroy', ['product' => $product]) }}" method="POST" onsubmit="return confirm('Are you sure wanted to delete it?')">
    <div class="btn-group float-end" role="group" aria-label="First group">
        
        <a title="View details" class="btn btn-outline-primary waves-effect waves-light btn-sm btn-icon" href="{{ route($entity.'.show', ['product' => $product]) }}"><i class="fa fa-search"></i> View</a> 
      
        <a title="Edit details" class="btn btn-outline-primary waves-effect waves-light btn-sm btn-icon" href="{{ route($entity.'.edit', ['product' => $product]) }}"><i class="fa fa-pencil-alt"></i> Edit</a>
      
        @csrf
        @method('DELETE')
        <button type="submit"  class="btn btn-outline-danger waves-effect waves-light btn-sm btn-icon"><i class="fa fa-trash-alt"></i> Delete</button>
       
    </div>
</form>