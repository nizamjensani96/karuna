@extends('layouts.master')

@section('title','Create product')

@section('content')

@if(session('success'))
<div class="alert alert-success">{{session('success')}}</div>
@endif

@if(session('error'))
<div class="alert alert-danger">{{session('error')}}</div>
@endif

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title mb-4 d-flex justify-content-between align-items-center">Show Product
                    <a href="{{route('products.index',)}}" class="btn btn-primary" title="Back to previous page"> <i class="bx bx-arrow-back"></i>Back</a>
                </h3>

                <table>
                    <tr>
                        <th>Name:</th>
                        <td>{{$product->name}}</td>
                    </tr>
                    <tr>
                        <th>Price:</th>
                        <td>{{$product->price}}</td>
                    </tr>
                    <tr>
                        <th>Details:</th>
                        <td>{{$product->details}}</td>
                    </tr>
                    <tr>
                        <th>Publish: </th>
                        @if($product->publish == 1)
                        <td> Yes</td>
                        @else
                        <td> No</td>
                        @endif
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection