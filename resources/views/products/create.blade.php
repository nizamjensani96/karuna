@extends('layouts.master')

@section('title','Create product')

@section('content')

@if(session('success'))
<div class="alert alert-success">{{session('success')}}</div>
@endif

@if(session('error'))
<div class="alert alert-danger">{{session('error')}}</div>
@endif

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title mb-4 d-flex justify-content-between align-items-center">Add New Product
                    <a href="{{route('products.index')}}" class="btn btn-primary" title="Back to previous page"> <i class="bx bx-arrow-back"></i>Back</a>
                </h3>

                <form action="{{ route('products.store') }}" method="POST">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="name" class="form-label">Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="price" class="form-label">Price <span class="text-danger">*</span></label>
                            <input type="number" class="form-control @error('price') is-invalid @enderror" id="price" name="price" step="0.01" min="0.01" required>
                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="name" class="form-label">Details</label>
                            <textarea id="details" name="details" cols="20" rows="5" class="form-control @error('details') is-invalid @enderror"></textarea>
                            @error('details')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="publish" class="form-label">Publish</label>
                            <div class="form-check">
                               
                                <input class="form-check-input" type="radio" name="publish" value="1" >
                                <label class="form-check-label" for="publish">
                                    Yes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="publish" value="0" checked>
                                <label class="form-check-label" for="publish">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="float-end">
                        <button type="submit" class="btn btn-primary w-md">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection