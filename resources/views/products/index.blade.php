@extends('layouts.master')



@section('title') Product @endsection

@section('content')

@if(session('success'))
<div class="alert alert-success">{{session('success')}}</div>
@endif

@if(session('error'))
<div class="alert alert-danger">{{session('error')}}</div>
@endif

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h3 class="card-title mb-4 d-flex justify-content-between align-items-center">Product List

                    <a href="{{ route('products.create') }}" class="btn btn-success"> <i class="bx bx-plus"></i> Create New Product</a>

                </h3>

                <div class="table-responsive-md">
                    <table class="table " id="datatables">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Name</th>
                                <th>Price (RM)</th>
                                <th>Details</th>
                                <th>Publish</th>
                                <th width="190" class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody >

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-bottom')
<script>
    $(document).ready(function() {
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{route('products.index')}}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'price',
                        name: 'price'
                    },
                    {
                        data: 'details',
                        name: 'details'
                    },
                    {
                        data: 'publish',
                        name: 'publish'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        className: 'text-center'
                    }
                ]
            });
        });
    });
</script>
@endsection